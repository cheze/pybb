#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys
import json
import getopt
from oca.pybb.pybb import Pybb
from jsonschema import validate
from urllib.request import urlopen
from jsonschema import ValidationError


VERSION = 0.1

XINETD_INSTALL = """
------------------------------------------------------------------
- xinetd template config,
------------------------------------------------------------------
service pybb
{
        type           = UNLISTED
        port           = 678
        socket_type    = stream
        protocol       = tcp
        wait           = no
        user           = root
        env            = PYTHONPATH=/home/sysop/pybb
        server         = <file path of pybb.py>
        server_args    = -c <filepath of config> --flat

        # configure the IP address(es) of your Nagios server here:
        #only_from      = 127.0.0.1 10.0.20.1 10.0.20.2

        # Don't be too verbose. Don't log every check. This might be
        # commented out for debugging. If this option is commented out
        # the default options will be used for this service.
        log_on_success =

        disable        = no
}
------------------------------------------------------------------

+ xinetd installation on centos:
   sudo yum install xinetd

+ xinetd configuration
   copy modified version of given config file to /etc/xinetd.d
   You must note that file permissions must be change:
     sudo chmod go-rwx /etc/xinetd.d/pybb

+ start xinetd:
   sudo systemctl start xinetd

+ start xinetd at boot time
   sudo systemctl enable xinetd
"""

CONFIG_SCHEMA_PATH = os.path.join(os.path.dirname(__file__), 'schemas', 'config_schema.json')


def xinetd_info():
    """
    Display sample xinetd configuration file and xinetd installation procedure
    """
    print(XINETD_INSTALL)


def usage():
    """
    Basic usage method
    """
    print(sys.argv[0], "  by Resif Team")
    print("Print on stdout a JSON representation of system informations")
    print("")
    print("Execution parameters")
    print("")
    print("-c, --config        Set configuration filepath (exclusive with --generate_config)")
    print("  -f, --flat          Output a flat dictionnary instead of structured one")
    print("")
    print("Config generation parameters")
    print("")
    print("--generate_config     Set the server code for configuration generation from Phoenix")
    print("  --database_host     Set Phoenix database host")
    print("")
    print("Common options")
    print("")
    print("-o, --output        Set an output filename instead of stdout")
    print("-h, --help          Display this message")
    print("--xinetd            Display xinetd sample config with some related information")
    print("-V, --version       Print version information.")


def config_validate(configuration):
    """
    Validate a given configuration with configuration schema.

    configuration -- The configuration to validate

    Raises:
      ValidationError if the instance is invalid
      SchemaError if the schema itself is invalid
    """
    schema_file = open(CONFIG_SCHEMA_PATH)
    schema = json.load(schema_file)
    validate(configuration, schema)


def load_config(filepath):
    try:
        f = open(filepath)
        config = json.load(f)
        config_validate(config)
    except IOError as exception:
        sys.stderr.write("Can't open configuration file %s: %s\n" % (filepath, exception))
        sys.exit(1)
    except ValueError as exception:
        sys.stderr.write("Can't parse configuration file %s: %s\n" % (filepath, exception))
        sys.exit(1)
    except ValidationError as exception:
        sys.stderr.write("Invalid configuration structure: %s\n" % exception)
        sys.exit(1)
    return config


def flat_dictionnary(dictionnary):
    result = {}
    for prefix, subdict in dictionnary.items():
        for key, value in subdict.items():
            result["%s.%s" % (prefix, key)] = value
    return result


def main():
    config_filepath = None
    server_code = None
    database_host = None
    output_filename = None
    flat_mode = False
    try:
        opts, _ = getopt.getopt(sys.argv[1:], "c:o:fhV",
                                ["config=", "output=", "flat",
                                 "generate_config=", "database_host=",
                                 "help", "xinetd", "version"])
    except getopt.GetoptError as err:
        sys.stderr.write("Error: %s\n" % err)
        usage()
        sys.exit(1)

    for current_option, current_argument in opts:
        if current_option in ("-c", "--config"):
            config_filepath = current_argument
        elif current_option in ("-h", "--help"):
            usage()
            sys.exit()
        elif current_option in ("--xinetd",):
            xinetd_info()
            sys.exit()
        elif current_option in ("-o", "--output"):
            output_filename = current_argument
        elif current_option in ("-f", "--flat"):
            flat_mode = True
        elif current_option in ("--generate_config",):
            server_code = current_argument
        elif current_option in ("--database_host",):
            database_host = current_argument
        elif current_option == "-V":
            print(sys.argv[0], " version ", VERSION)
            sys.exit(1)
        else:
            assert False, "unhandled option % s" % current_option

    if config_filepath is None and server_code is None:
        sys.stderr.write("You must specify configuration file with -c or server config file generation  with --generate_config\n")
        sys.exit(1)

    if config_filepath is not None and server_code is not None:
        sys.stderr.write("-c and --generate_config are exclusive\n")
        sys.exit(1)

    if config_filepath is not None and database_host is not None:
        sys.stderr.write("--database_host need --generate_config\n")
        sys.exit(1)

    # Initalize output file
    output_file = sys.stdout
    if output_filename is not None:
        try:
            output_file = open(output_filename, "w")
        except Exception as exception:
            sys.stderr.write("Can't open output file: %s\n" % exception)
            sys.exit(1)

    if config_filepath:
        # Pybb execution according to given config file
        config = load_config(config_filepath)

        # Create Pybb
        pybb = Pybb()

        # Declare plugin in Pybb
        for plugin in config["plugins"]:
            # Load plugin
            try:
                pybb.add_plugin(plugin["name"], plugin["entry"], plugin["classpath"], plugin["args"])
            except Exception as exception:
                sys.stderr.write("Can't add plugin %s: %s\n" % (plugin["name"], exception))
                sys.exit(1)

        result = pybb.run()
        if flat_mode:
            result = flat_dictionnary(result)
        json.dump(result, output_file, indent=2)
    elif server_code is not None:
        # Get configuration from Phoenix
        phoenix_url = "http://%s/ws-phoenix-dev/pybb/config?server_code=%s" % (database_host, server_code)
        result_str = urlopen(phoenix_url).read().decode('utf8')
        result = json.loads(result_str)
        if 'status' not in result:
            sys.stderr.write("Can't find status of response")
            sys.exit(1)
        if result['status'] == 'error':
            sys.stderr.write("Error during config generation: %s" % result["message"])
            sys.exit(1)
        generated_config = result["result"]
        json.dump(generated_config, output_file, indent=2)
    else:
        sys.stderr.write("Internal error can't be here\n")
        sys.exit(1)


if __name__ == "__main__":
    main()
