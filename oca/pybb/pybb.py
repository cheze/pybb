# -*- coding: utf-8 -*-
import traceback


class Pybb(object):

    def __init__(self):
        """
        Initialize this Pybb
        """
        # Associate plugin_entry with plugin information
        # The information have the following structure:
        # {
        #   "name": The plugin name
        #   "method": The method producing data for this plugin
        #   "args": The method producing data arguments
        # }
        self.__plugins = dict()

    def __load_plugin(self, class_path):
        """
        Load class constructor according to given class path.

        class_path -- The import path of the class

        return     -- The class constructor

        This method exit in case of error
        """
        className = class_path.split(".")[-1]
        moduleName = class_path.split(".")[:-1]
        fromlist = []
        if len(class_path.split(".")) > 2:
            fromlist.append(".".join(class_path.split(".")[:-2]))
        module = __import__(".".join(moduleName), fromlist=[fromlist])
        return getattr(module, className)

    def add_plugin(self, plugin_name, plugin_entry, plugin_classpath, method_args):
        """
        Add a plugin.

        plugin_name      -- The name of the plugin
        plugin_entry     -- The entry in the output dictionnary
        plugin_classpath -- The plugin classpath
        method_args      -- The arguments given to plugin method
                            (as list)
        """
        # Load plugin class and create plugin
        plugin_constructor = self.__load_plugin(plugin_classpath)
        plugin = plugin_constructor()
        if plugin_entry in self.__plugins:
            raise ValueError("PLugin %s have the same entry than plugin %s (%s)" %
                             (plugin_name, self.__plugins[plugin_entry]["name"],
                              plugin_entry))

        # Update plugin dictionnary
        self.__plugins[plugin_entry] = {
            "name": plugin_name,
            "method": plugin.run,
            "args": method_args
        }

    def run(self):
        """
        Give system information according to loaded plugins

        return -- The system information as JSON compatible dictionnary
        """
        result = dict()

        for plugin_entry, plugin_def in self.__plugins.items():
            try:
                result[plugin_entry] = plugin_def["method"](plugin_def["args"])
            except Exception as exception:
                result[plugin_entry] = {
                    "plugin_error": True,
                    "plugin_message": "Plugin %s error: %s" % (plugin_def["name"], exception),
                    "plugin_taceback": traceback.format_exc(10)
                }
        return result
