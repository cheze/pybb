# -*- coding: utf-8 -*-
import psutil
from oca.pybb.abstract_plugin import AbstractPlugin


class MemoryPlugin(AbstractPlugin):

    def run(self, param):
        """
        Give a key/value dictionnary with memory informations

        param -- Not used for this plugin

        return -- The memory informations as key/value dictionnary
        """
        result = {}
        memory_info = psutil.virtual_memory()
        result["total"] = memory_info.total
        result["available"] = memory_info.available
        result["used"] = memory_info.used
        result["free"] = memory_info.free
        result["active"] = memory_info.active
        result["inactive"] = memory_info.inactive
        result["buffers"] = memory_info.buffers
        result["cached"] = memory_info.cached
        result["shared"] = memory_info.shared
        result["slab"] = memory_info.slab
        result["plugin_message"] = "Memory plugin ok"
        result["plugin_error"] = False

        return result
