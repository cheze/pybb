# -*- coding: utf-8 -*-
import psutil
import functools
from oca.pybb.abstract_plugin import AbstractPlugin


class CpuPlugin(AbstractPlugin):

    def run(self, param):
        """
        Give a key/value dictionnary with CPU informations

        param -- Not used for this plugin

        return -- The CPU informations as key/value dictionnary
        """
        result = {}

        # Number of CPU
        result["count"] = psutil.cpu_count()

        # Global CPU percent usage
        result["usage"] = psutil.cpu_percent(interval=1)

        # Global usage repartion (user, nice, system, idle, iowait, irq, softirq)
        usage_infos = psutil.cpu_times(percpu=False)
        total_time = functools.reduce(lambda a, e: a + e, usage_infos, 0)
        result["percent_user"] = usage_infos.user / total_time
        result["percent_nice"] = usage_infos.nice / total_time
        result["percent_system"] = usage_infos.system / total_time
        result["percent_idle"] = usage_infos.idle / total_time
        result["percent_iowait"] = usage_infos.iowait / total_time
        result["percent_irq"] = usage_infos.irq / total_time
        result["percent_softirq"] = usage_infos.softirq / total_time
        result["plugin_message"] = "CPU plugin ok"
        result["plugin_error"] = False

        return result
