# -*- coding: utf-8 -*-
import psutil
from oca.pybb.abstract_plugin import AbstractPlugin


class DiskPlugin(AbstractPlugin):

    def __process_one_disk(self, disk_usage, prefix):
        result = {}
        result["%s.percent" % prefix] = disk_usage.percent
        result["%s.total" % prefix] = disk_usage.total
        result["%s.used" % prefix] = disk_usage.used
        result["%s.free" % prefix] = disk_usage.free
        return result

    def run(self, param):
        """
        Give a key/value dictionnary with CPU informations

        param -- Associate partition mount point with code used in result

        return -- The Disk informations as key/value dictionnary
        """
        result = {}

        # Get the list of mounted partitions
        partitions = psutil.disk_partitions()
        for current in partitions:
            current_usage = psutil.disk_usage(current.mountpoint)
            if current.mountpoint in param:
                current_result = self.__process_one_disk(current_usage, param[current.mountpoint])
                result.update(current_result)

        result["plugin_message"] = "Disk plugin ok"
        result["plugin_error"] = False

        return result
