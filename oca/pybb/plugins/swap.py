# -*- coding: utf-8 -*-
import psutil
from oca.pybb.abstract_plugin import AbstractPlugin


class SwapPlugin(AbstractPlugin):

    def run(self, param):
        """
        Give a key/value dictionnary with swap informations

        param -- Not used for this plugin

        return -- The swap informations as key/value dictionnary
        """
        result = {}
        swap_info = psutil.swap_memory()
        result["total"] = swap_info.total
        result["used"] = swap_info.used
        result["free"] = swap_info.free
        result["plugin_message"] = "Swap plugin ok"
        result["plugin_error"] = False

        return result
