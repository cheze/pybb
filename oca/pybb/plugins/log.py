
# -*- coding: utf-8 -*-
import os
import json
from oca.log.line_parser import LogLineFormatParser
from oca.pybb.abstract_plugin import AbstractPlugin


class LogPlugin(AbstractPlugin):

    def __init_loggile(self, param):
        """
        Give log file according to information present
        in retention file. The retention file content
        store log_file informations during last call.

        param     -- The plugin parameters
        retention -- The retention file path content
                     as a dictionnary like
                     {
                         "realpath": <The canonical log_filepath>,
                         "inode": <The log file inode>,
                         "seek": <The seek value>
                     }
        """
        realpath = os.path.realpath(param["log_filepath"])
        if os.path.exists(param["retention"]):
            #
            # Open retetion file, with the following structure
            # {
            #   "realpath": <The log file realpath>, (canonical form),
            #   "seek": <The seek file value from begin>,
            #   "inode": <The file inode>
            # }
            #
            retention = json.load(open(param["retention"]))
            if realpath != retention["realpath"]:
                # link change don't load retention
                return open(realpath)
            if os.stat(realpath).st_ino != retention["inode"]:
                # Same name but not same file
                return open(realpath)
            # Same file that during last launch
            result = open(realpath)
            result.seek(retention["seek"])
            return result
        else:
            # First time we parse log file
            # (no retention file)
            return open(param["log_filepath"])
        return result

    def __save_retention_file(self, retention_filepath, logfile):
        """
        Save retention file for next launch

        retention_filepath -- The retention filepath
        logfile            -- The logfile after parsing
        """
        retention_content = {
            "realpath": os.path.realpath(logfile.name),
            "seek": logfile.tell(),
            "inode": os.fstat(logfile.fileno()).st_ino
        }
        f = open(retention_filepath, "w")
        json.dump(retention_content, f)
        f.close()

    def run(self, param):
        """
        Run this MemoryPlugin

        param -- The plugin parameters as dictionnary like:
                 {
                     "log_filepath": <The logging filepath to parse>,
                     "format": <The logging line format>,
                     "valid_id": <The list of id used in log line format>,
                     "retention": <The retention file path>,
                     "level": <The minimum level to notify line>
                 }
                 The ordered log level are:
                 DEBUG, INFO, WARNING, ERROR
        """
        all_level = ["DEBUG", "INFO", "WARNING", "ERROR"]
        notify_level = all_level
        if param["level"] in all_level:
            notify_level = all_level[all_level.index(param['level']):]
        line_parser = LogLineFormatParser(param["valid_id"])
        previous_result = None
        to_notify = list()
        logfile = self.__init_loggile(param)
        for current_line in logfile:
            # Parse the current line
            parse_result = line_parser.parse_line(current_line, param["format"])
            if parse_result is None:
                # Attach this line with previous line (pyybb_continuation).
                if previous_result is not None:
                    previous_result["pyybb_continuation"] += "\n%s" % current_line
                continue
            # Append result if need
            if parse_result['level'] in notify_level:
                to_notify.append(parse_result)
            parse_result["pyybb_continuation"] = ""
            previous_result = parse_result

        result = dict()
        result["plugin_message"] = "Log plugin ok"
        result["plugin_error"] = False
        # Init result
        for current in all_level:
            if current in notify_level:
                result[current] = 0
            else:
                result[current] = None
        # Store result according to parse
        for current in to_notify:
            result[current["level"]] += 1

        # Try to save retention file
        try:
            self.__save_retention_file(param["retention"], logfile)
        except Exception as exception:
            result["plugin_error"] = True
            result["plugin_message"] = "Can't save retention file: %s" % exception

        return result
