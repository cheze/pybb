# -*- coding: utf-8 -*-
from abc import ABC
from abc import abstractclassmethod


class AbstractPlugin(ABC):

    @abstractclassmethod
    def run(self, param):
        pass
